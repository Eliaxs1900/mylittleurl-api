/*
    //\\\\\\\\\\\\\\\\\\//////////////////////
    || author: Elías Sebastián Martín Ortiz ||
    ////////////////////\\\\\\\\\\\\\\\\\\\\\\
    
*/

// Variables globales
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const tools = require('./controllers/tools');
const fileUpload = require('express-fileupload');

// Varibles de la base de datos
var dbName = 'MyLittleURL';
// var host = 'LenovoG50-80';
var host = 'localhost';
var hostPort = '27017';
var username = ''; // Si se deja en blanco no se usará
var password = ''; // Si se deja en blanco no se usará
var dbURI = '';

console.log('\x1b[34m%s\x1b[0m', 'Iniciando API');
console.log('\x1b[35m%s\x1b[0m', 'Autor: Elías Masrtín'); console.log();

console.log('\x1b[33m%s\x1b[0m', 'Interfaces de red:');
tools.interfaces(); console.log();

///////////////////////////////////////////////////////////
// Base de datos - Configuración y conexión
console.log('\x1b[33m%s\x1b[0m','Configuración mongoose:');

if(username === '') {
    dbURI = 'mongodb://' + host + ':' + hostPort + '/' + dbName;
} else if ((username != '') && (password === '')) {
    dbURI = 'mongodb://' + username + '@' + host + ':' + hostPort + '/' + dbName;
} else {
    dbURI = 'mongodb://' + username + ':' + password + '@' + host + ':' + hostPort + '/' + dbName;
}

console.log('\x1b[35m%s\x1b[0m','URI para MongoDB: ' + dbURI); console.log();

mongoose.connect(dbURI, {useNewUrlParser: true}); //connect

// Configuración de los eventos de la conexión Mongoose
mongoose.connection.on('connected', function (){
    console.log('\x1b[32m%s\x1b[0m','Conexión mongoose establecida');
});

mongoose.connection.on('error', function() {
    console.log('\x1b[31m%s\x1b[0m','Conexión mongoose cerrada');
});

mongoose.connection.on('disconected', function() {
    console.log('Mongoose default connection disconected');
})

// Si el proceso 'Node termina, se cierra la conexión Mongoose
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        console.log('Conexión Mongoose desconectada a través de la aplicación');
        process.exit(0);
    });
});

///////////////////////////////////////////////////
// Creamos la aplicación express y la configuramos
console.log('\x1b[33m%s\x1b[0m','Configuración express:')
var app = express();

//Configurarción del bodyParser para que convierta el body de nuestras peticiones a JSON
app.use(bodyParser.json());
//📌 Comprobar no me fio de que capture json errors
app.use(function (error, req, res, next){
    //Catch bodyParser error
    if (error.message === "invalid json"){
        sendError (res, myCustomErrorMessage);
    }else{
        next ();
    }
});
app.use(cors());

// default options
app.use(fileUpload());

// Hello World ✔
app.get('/api', (req, res) => {
    console.log('\x1b[32m%s\x1b[0m','[Hola Mundo], Fecha: ' + new Date());
    return res.status(200).json({message: 'Hola Mundo'});
});

app.use('/api', function(req, res, next) {
    
    var contype = req.headers['content-type'];
    if (!contype || !((contype.indexOf('application/json') == 0) || (contype.indexOf('multipart/form-data') == 0))) {

        return res.status(400).json({
            reason: 'Content-Type expected',
            message: 'All requests to this API must add the Content-Type header equal to application/json' 
            + ' or multipart/form-data for images',
            extra: 'Actual Content-Type is: ' + contype
        }) 
    }

    next()
});

require('./routes/routes')(app);

// Little message for rest of querys.
app.get('/*', (req, res) => {
    return res.status(404).json({message: 'Welcome to MyLittleURL API!. Please visit \'/api\' route'})
})

const port = process.emit.port || '3000';
console.log('Puerto asignado: ' + port); console.log();

console.log('\x1b[35m%s\x1b[0m','Si la conexión con la base de datos tiene éxito\n'
                              + 'se creará el servidor Express'); console.log();

if(mongoose.connection.on('connected', function() {
    app.listen(port, () => {
        console.log('\x1b[34m%s\x1b[0m', 'Express corriendo en http://localhost:' + port + '\n');
    });
}));