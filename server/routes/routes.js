const tools = require('../controllers/tools');
const usuarioCtrl = require('../controllers/usuario');
const enlaceCtrl = require('../controllers/enlace');
const visitaCtrl = require('../controllers/visita');

module.exports = function(app) {

    ////////////////////////////
    //  Client Rest          //
    //////////////////////////

    app.route('/api/usuarios/signin')
    .post(usuarioCtrl.signin) // Status ✔

    app.route('/api/usuarios/signup')
    .post(usuarioCtrl.signup) // Status ✔

    app.route('/api/usuarios')
    .post(tools.checkAuth, usuarioCtrl.update) // Status ✔
    .delete(tools.checkAuth, usuarioCtrl.delete) // Status ✔

    app.route('/api/usuarios/imagen')
    .get(usuarioCtrl.getPhoto) // Status ✔
    .post(tools.checkAuth, usuarioCtrl.newPhoto) // Status ✔
    .delete(tools.checkAuth, usuarioCtrl.removePhoto); // Status ✔
    
    app.route('/api/enlaces')
    .get(tools.checkAuth, enlaceCtrl.getAll) // Status ✔
    .post(tools.checkAuth, enlaceCtrl.create) // Status ✔
    .delete(tools.checkAuth, enlaceCtrl.daleteAll) // Status ✔

    app.route('/api/enlaces/:id')
    .get(tools.checkAuth, enlaceCtrl.get) // Status ✔
    .put(tools.checkAuth, enlaceCtrl.update) // Status ✔
    .delete(tools.checkAuth, enlaceCtrl.delete) // Status ✔

    ////////////////////////////
    //  Admininstrator Rest  //
    //////////////////////////

    app.route('/api/visitas')
    .get(tools.checkAuth, visitaCtrl.getAll) // Status ❌
    .delete(tools.checkAuth, tools.isAdmin, visitaCtrl.daleteAll); // Status ❌

    app.route('/api/visitas/:id')
    .get(tools.checkAuth ,visitaCtrl.get) // Status ❌
    .delete(tools.checkAuth, visitaCtrl.delete); // Status ❌

    ////////////////////////////
    //  Special Rest         //
    //////////////////////////

    app.route('/api/new-world') // ⚡ WARNING, ONLY ADMIN ⚡ | Status ❌

    app.route('/:uri') //Status ✔
    .get(visitaCtrl.redirect)
}
