var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var visitaSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    ip: { type: String, required: true },
    ip_data: { //Esto se rellena desde ipStack.com
        continent_code: { type: String, default: null },
        continent_name: { type: String, default: null },
        country_code: { type: String, default: null },
        country_name: { type: String, default: null },
        region_code:  { type: String, default: null },
        region_name:  { type: String, default: null },
        city: { type: String, default: null },
        zip: { type: String, default: null },
        latitude: { type: Number, default: null },
        longitude: { type: Number, default: null },
        location: {
            capital: { type: String, default: 'null' },
            languages: [mongoose.Schema({
               code: { type: String, default: 'null' },
               name: { type: String, default: 'null' },
               native: { type: String, default: 'null' },
            },{ _id : false })],
            country_flag: { type: String, default: 'null' }
        },
    },
    dispositivo: String,
    os: String,
    platform: String,
    user_agent: { type: String, required: true }
});

module.exports = mongoose.model('Visita', visitaSchema);
