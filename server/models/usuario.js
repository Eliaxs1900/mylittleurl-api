var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre: { type: String, required: true },
    apellidos: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    telefono: { type: String, default: null },
    tipoUsuario: {
        type: String,
        enum: ['admin', 'freemium'],
        default: "freemium"
    },
    imagen: { type: String, default: function() { return Math.floor(Math.random()*3) }}
});

module.exports = mongoose.model('Usuario', usuarioSchema);
