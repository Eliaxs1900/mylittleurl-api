var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var enlaceSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    enlace: { type: String, required: true },
    descripcion: { type: String, required: true },
    visitas: [{ type: Schema.ObjectId, ref: 'Visita' }],
    idUsuario: { type: Schema.ObjectId, required: true },
    shortLink: String
});

module.exports = mongoose.model('Enlace', enlaceSchema);
