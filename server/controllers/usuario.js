const Usuario = require('../models/usuario')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const dateFormat = require('dateformat')
const tools = require('../controllers/tools')
const path = require('path')
const fs = require('fs')
const uploadPath = path.join(__dirname, '..', 'assets', 'images')


exports.signup = function (req, res) {
    if (!req.body.nombre | !req.body.apellidos | !req.body.email | !req.body.password) {
        return res.status(400).json({
            status: 'fail',
            data: 'The request must have nombre, apellidos, email & password'
        });
    }

    Usuario.findOne({ email: req.body.email }, function (err, userFound) {
        if (err) { return res.status(500).json({ status: 'error', data: 'Error with Database' }) }
        if (userFound) { return res.status(400).json({ status: 'fail', data: 'This email has already used' }) }

        //Generate new Usuario Model
        const usuario = new Usuario({
            _id: new mongoose.Types.ObjectId(),
            nombre: req.body.nombre,
            apellidos: req.body.apellidos,
            email: req.body.email,
        });
        //Hash the password
        bcrypt.hash(req.body.password, 10, function (err, hash) {
            if (err) { return res.status(500).json({ status: 'error', data: err }) }

            usuario.password = hash;

            //save on MongoDB
            usuario.save(function (err) {
                if (err) return res.status(500).json({
                    status: 'error',
                    error: err
                });

                usuario.password = '';
                token = tools.jwtHashData(usuario)

                return res.status(201).json({
                    status: 'success',
                    data: token
                })
            })
        })
    })

    console.log('\x1b[33m%s\x1b[0m', '[Usuario] in ' + dateFormat(new Date(), 'dd-mm "at" hh:mm:ss'))
}

exports.signin = function (req, res) {
    if (!req.body.email | !req.body.password) {
        return res.status(400).json({
            status: 'fail', data: 'The request must have email & password'
        });
    } else { // body contains "email" and "password"
        Usuario.findOne({ email: req.body.email }, function (err, usuario) {
            if (err) { return res.status(500).json({ status: 'error', data: err }) }

            if (usuario) { //return User
                bcrypt.compare(req.body.password, usuario.password).then((result) => {
                    if (result) {
                        usuario.password = '';
                        return res.status(200).json({
                            status: 'success',
                            data: tools.jwtHashData(usuario)
                        });
                    }

                    return res.status(401).json({ status: 'fail', data: 'Wrong password' });
                });
            } else {
                return res.status(404).json({ status: 'fail', data: "Email was not found" });
            }
        });
    };

    console.log('\x1b[32m%s\x1b[0m', '[Usuario] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));
}

checkIntegrityArray = function(expected, actual) {
    let status = true;
    actual.forEach(element => {
        isFinded = false;
        expected.forEach(element2 => {
            if (element === element2) {
                isFinded = true;
            }
        })
        if(!isFinded) { status = false; }
    })
    return status;
}

exports.update = function (req, res) {
    expected = ['nombre', 'apellidos', 'email', 'password', 'telefono', 'tipoUsuario']
    if(!checkIntegrityArray(expected, Object.keys(req.body))) {
        return res.status(400).json({
            status: 'fail',
            data: 'The request must have nombre, apellidos, email, password & telefono'
        });
    } else {
        const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization);

        Usuario.findById(headerTokenDecoded.data._id, function (err, usuario) {
            if (err) {
                return res.status(500).json({ status: 'fail', data: err })
            }

            if (usuario) {
                Object.keys(req.body).forEach((key, index) => {
                    if(key !== 'password') {
                        // console.log(usuario[key]);
                        Object.values(req.body).forEach((value, index2 )=> {
                            if(index === index2) {
                                usuario[key] = value;
                            }
                        });
                    } else {
                        Object.values(req.body).forEach((value, index2 )=> {
                            if(index === index2) {
                                bcrypt.hash(value, 10, function (err, hash) {
                                    usuario[key] = hash
                                    usuario.save()
                                })
                            }
                        });
                    }
                })

                usuario.save((saveErr, updateUser) => {
                    if (updateUser) {
                        updateUser.password = "";

                        return res.status(201).json({
                            status: 'success',
                            data: tools.jwtHashData(usuario)
                        })
                    }
                })

            } else {
                return res.status(400).json({ status: 'fail', data: 'No user for this token' })
            }
        })
    }

    console.log('\x1b[34m%s\x1b[0m', '[Usuario] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'))
}

exports.delete = function (req, res) {
    const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization)

    if (headerTokenDecoded.data.tipoUsuario === 'admin') {
        return res.status(403).json({
            status: 'fail',
            data: 'An administrator can not delete his account'
        })
    }

    Usuario.findByIdAndDelete(headerTokenDecoded.data._id, function (err, user) {
        if (err) { return res.status(500).json({ status: 'error', message: err }) }

        if (user) {
            user.password = null
            res.status(200).json({ status: 'success', data: user })
            if (tools.fileExist(path.join(uploadPath, user.imagen))) {
                fs.unlink(path.join(uploadPath, user.imagen), (err) => { if (err) { throw err; next() } })
            }
        } else {
            return res.status(500).json({ status: 'error', data: 'No user for this token' })
        }
    })

    console.log('\x1b[31m%s\x1b[0m', '[Usuario] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'))
}

exports.getPhoto = function (req, res) {
    const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization);
    if (
        (headerTokenDecoded.data.imagen != 0) &
        (headerTokenDecoded.data.imagen != 1) &
        (headerTokenDecoded.data.imagen != 2)
    ) {
        if (tools.fileExist(path.join(uploadPath, headerTokenDecoded.data.imagen))) {
            return res.status(200).sendFile(path.join(uploadPath, headerTokenDecoded.data.imagen))
        } else {
            return res.status(404).json({ status: 'fail', data: 'This uri is broken' })
        }
    } else {
        return res.status(404).json({ status: 'fail', data: 'This user have not an image profile' })
    }
}

exports.newPhoto = function (req, res) {
    const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization)
    //Special part (Default Avatars)
    if (req.body.defaultImage) {
        imageAvatar = req.body.defaultImage
        if ((imageAvatar == '0') | (imageAvatar == '1') | (imageAvatar == '2') | (imageAvatar == '3')) {

            Usuario.findById(headerTokenDecoded.data._id, function (err, usuario) {
                if (err) { return res.status(500).json({ status: 'fail', data: err }) }
                if (usuario) {
                    if (tools.fileExist(path.join(uploadPath, usuario.imagen))) {
                        fs.unlink(path.join(uploadPath, usuario.imagen), (err) => { if (err) { throw err; next() } })
                    }
                    usuario.imagen = imageAvatar;
                    res.status(200).json({ status: 'success', data: tools.jwtHashData(usuario) });

                    return usuario.save();
                } else {
                    return res.status(400).json({ status: 'fail', data: 'No user for this token' })
                }
            })
        } else {
            return res.status(400).json({ status: 'fail', data: 'defaultImage must be \'0\', \'1\', \'2\' or \'3\'' })
        }
    } else {
        //Imagen 📷
        if (!req.files || !req.files.imageAvatar) {
            return res.status(300).json({
                status: 'fail', data: 'The request must have imageAvatar file'
            })
        }
        
        imageAvatar = req.files.imageAvatar
        name = imageAvatar.md5 + path.extname(imageAvatar.name)
        name = tools.random() + '-' + name

        imageAvatar.mv(path.join(uploadPath, name), function (err) {
            if (err) { return res.status(500).send(err) }

            Usuario.findById(headerTokenDecoded.data._id, function (err, usuario) {
                if (err) { return res.status(500).json({ status: 'fail', data: err }) }

                if (usuario) {
                    if ((usuario.imagen == 0) | (usuario.imagen == 1) | (usuario.imagen == 2) | (usuario.imagen == 3)) {
                        usuario.imagen = name;

                    } else {
                        fs.unlink(path.join(uploadPath, usuario.imagen), (err) => { if (err) { throw err; next() } })
                        usuario.imagen = name
                    }

                    usuario.save((saveErr, updateUser) => {
                        if (updateUser) {
                            updateUser.password = "";
                            return res.status(201).json({
                                status: 'success',
                                data: tools.jwtHashData(updateUser),
                                imagen: usuario.imagen
                            })
                        }
                    })

                } else {
                    return res.status(400).json({ status: 'fail', data: 'No user for this token' })
                }
            })
        })
    }
    console.log('\x1b[33m%s\x1b[0m', '[Usuario] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'))
}

exports.removePhoto = function (req, res) {
    const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization)

    Usuario.findById(headerTokenDecoded.data._id, function (err, usuario) {
        if (err) { return res.status(500).json({ status: 'fail', data: err }) }

        if (usuario) {
            if ((usuario.imagen == 0) | (usuario.imagen == 1) | (usuario.imagen == 2)) {
                return res.status(500).json({ status: 'fail', data: 'This user has no photo' });
            } else
                fs.unlink(path.join(uploadPath, usuario.imagen), (err) => { if (err) { throw err; next() } })

            usuario.imagen = Math.floor(Math.random() * 3)

            usuario.save((saveErr, updateUser) => {
                if (updateUser) {
                    updateUser.password = "";

                    return res.status(201).json({
                        status: 'success',
                        data: tools.jwtHashData(updateUser)
                    })
                }
            })
        } else {
            return res.status(400).json({ status: 'fail', data: 'No user for this token' })
        }
    });
}