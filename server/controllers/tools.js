const jwt = require('jsonwebtoken')
const Hashids = require('hashids')
const axios = require('axios')
const os = require('os')
const fs = require('fs')
const uaParser = require('ua-parser-js'); //First
const useragent = require('express-useragent'); //For Unknow
const ipstackKey = '43116d0633714a2ed8f6b52b900448e7'
const clave = 'ec0cdbbd27374d6ba9299121f2d7cbe1'
const tiempoExpiracion = '7d'
const ifaces = os.networkInterfaces()

exports.interfaces = function () {
    Object.keys(ifaces).forEach((ifname) => {
        alias = 0;

        ifaces[ifname].forEach((iface) => {
            if ('IPv4' !== iface.family || iface.internal !== false) {
                // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                return;
            }

            if (alias >= 1) { //this single interface has multiple ipv4 addresses
                console.log(ifname + ':' + alias, iface.address);
            } else { //this interface has only one ipv4 adress
                console.log(ifname, iface.address);
            }
            ++alias;
        })
    })
}

//middleware for check token status
exports.checkAuth = function (req, res, next) {
    if (!req.headers.authorization) {
        return res.status(403).json({
            reason: 'No Auth token provided',
            message: 'Authorization header is needed'
        })
    }

    jwt.verify(req.headers.authorization, clave, function (err) {
        if (err) {
            if (err.name === 'TokenExpiredError') {
                return res.status(401).json({
                    reason: 'Token error',
                    message: err.message
                })
            }

            if (err.name === 'JsonWebTokenError' | 'NotBeforeError') {
                return res.status(400).json({
                    reason: 'Token error',
                    failed: err.message
                })
            }
        }

        next()
    });
}

//Check auth middleware must be probided after.
exports.isAdmin = function (req, res, next) {
    decoded = jwt.decode(req.headers.authorization);

    if (decoded.data.tipo_usuario !== 'admin') {
        return res.status(403).json({
            failed: 'you aren\'t admin'
        })
    }
    return next();
}

exports.checkParams = function (params, expected, res, next) {
    response = []
    search = false;
    for (params of expected) {
        expected.forEach((e1) => {
            if (params == e1) { search = true }
        })

        if (!search) {
            response.push(params)
        }
    }

    if (response.length != 0) {
        return res.status(400).json({
            status: "fail",
            data: response
        })
    } else {
        next()
    }
}

exports.typeOfDevice = function (userAgent) {
    device = uaParser(userAgent).device.type
    try {
        if (device === undefined) {
            if (useragent.parse(userAgent).isDesktop)
                device = 'desktop'
            if (useragent.parse(userAgent).isTablet)
                device = 'tablet'
            if (useragent.parse(userAgent).isMobile)
                device = 'mobile'
            if (useragent.parse(userAgent).isBot)
                device = 'bot'
        }
        if (device === undefined)
            device = 'other'
    } catch (error) {
        device = 'other'
    }
    return device
}

exports.getGeneric = function (userAgent) {
    device = uaParser(userAgent).device.type
    try {
        if (device === undefined) {
            if (useragent.parse(userAgent).isDesktop)
                device = 'desktop'
            if (useragent.parse(userAgent).isTablet)
                device = 'tablet'
            if (useragent.parse(userAgent).isMobile)
                device = 'mobile'
            if (useragent.parse(userAgent).isBot)
                device = 'bot'
        }
        if (device === undefined)
            device = 'other'
    } catch (error) {
        device = 'other'
    }
    return device
}

exports.getPlatform = function (userAgent) {
    device = undefined;
    try {
        device = useragent.parse(userAgent).platform;

    } catch (error) {
        device = 'other'
    }
    return device
}

exports.getOS = function (userAgent) {
    device = undefined;
    try {
        device = uaParser(userAgent).os;

    } catch (error) {
        device = 'other'
    }
    return device
}

exports.getModel = function(userAgent) {
    device = undefined;
    try {
        device = uaParser(userAgent).device.model
    } catch (error) {
        device = 'other'
    }
    return device
}

exports.jwtHashData = function (data) {
    return jwt.sign({ data }, clave, { expiresIn: tiempoExpiracion });
}

exports.jwtDecodeData = function (data) {
    return jwt.decode(data)
}

exports.apify = async function () {
    json = await axios.get('https://api.ipify.org/?format=json')
    return json.data.ip
}

exports.ipData = async function (ip) {
    uri = 'http://api.ipstack.com/' + ip + '?access_key=' + ipstackKey
    json = await axios.get(uri)
    return json.data
}

exports.shortLink = function (req) {
    hashids = new Hashids(req);
    return hashids.encode(3, 14, 15)
}

exports.random = function () {
    cadena = (Math.random() * 900000000300000000000) + 1000000000000000 + ''
    hashids = new Hashids(cadena)
    return hashids.encode(3, 14, 15)
}

exports.fileExist = function (path) {
    try {
        if (fs.existsSync(path)) {
            return true;
        }
    } catch (err) {
        return false
    }
}