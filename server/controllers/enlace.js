const Enlace = require('../models/enlace')
const Visita = require('../models/visita')
const Usuario = require('../models/usuario')
const mongoose = require('mongoose')
const dateFormat = require('dateformat')
const tools = require('./tools')

exports.getAll = function (req, res) {
    const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization);
    let populateSelection;

    populateSelection =
        'ip_data.continent_name ' +
        'ip_data.country_name ' +
        'ip_data.region_name ' +
        'user_agent'

    Enlace.find({ idUsuario: headerTokenDecoded.data._id }).populate('visitas', populateSelection).lean().exec(
        function (err, enlaces) {
            if (err) return res.status(404).json({ status: 'fail', data: err })

            if (enlaces) {
                //cuenta las visitas y las añade a un nuevo atributo 'clicks' del enlace
                enlaces.forEach((enlace) => {
                    enlace['clicks'] = enlace.visitas.length
                    // enlace['creation_date'] = enlace._id.getTimestamp()
                    enlace['creation_date'] = enlace._id.getTimestamp().getTime();
                    delete enlace['idUsuario'] //More Privacity
                    enlace.visitas.forEach((visita) => {
                        visita['device'] = tools.typeOfDevice(visita.user_agent)
                        visita['generic'] = tools.getGeneric(visita.user_agent)
                        visita['platform'] = tools.getPlatform(visita.user_agent)
                        visita['os'] = tools.getOS(visita.user_agent)
                        visita['model'] = tools.getModel(visita.user_agent)
                        visita['creation_date'] = visita._id.getTimestamp().getTime();
                        delete visita['user_agent'] //More Privacity
                        delete visita['_id'] //More Privacity
                    })
                })
                return res.status(200).json({ status: 'sucess', data: enlaces })
            } else {
                return res.status(404).json({ status: 'fail', data: 'There are not anything' })
            }
        })

    console.log('\x1b[32m%s\x1b[0m', '[Enlace] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));
}

exports.get = function (req, res) {
    const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization);
    let populateSelection;

    populateSelection =
        'ip_data.continent_name ' +
        'ip_data.country_name ' +
        'ip_data.region_name ' +
        'user_agent'


    Enlace.findById(req.params.id).populate('visitas', populateSelection).lean().exec(
        (err, enlace) => {
            if (err) { return res.status(400).json({ status: 'fail', data: 'Invalid Id' }) }

            if (enlace) {
                if (headerTokenDecoded.data._id == enlace.idUsuario) {
                    //cuenta las visitas y las añade a un nuevo atributo 'clicks' del enlace
                    enlace['clicks'] = enlace.visitas.length
                    enlace['creation_date'] = enlace._id.getTimestamp()
                    delete enlace['idUsuario'] //More Privacity
                    enlace.visitas.forEach((visita) => {
                        //Device
                        visita['device'] = tools.typeOfDevice(visita.user_agent)
                        visita['generic'] = tools.getGeneric(visita.user_agent)
                        visita['platform'] = tools.getPlatform(visita.user_agent)
                        visita['os'] = tools.getOS(visita.user_agent)
                        visita['model'] = tools.getModel(visita.user_agent)
                        visita['creation_date'] = visita._id.getTimestamp()
                        delete visita['user_agent'] //More Privacity
                        delete visita['_id'] //More Privacity
                    })
                    return res.status(200).json({ status: 'success', data: enlace })
                } else {
                    return res.status(404).json({ status: 'fail', data: 'You aren\t owner of this enlace' })
                }
            } else {
                return res.status(404).json({ status: 'fail', data: 'No enlaces for that Id' })
            }
        }
    )

    console.log('\x1b[32m%s\x1b[0m', '[Enlace] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));
}

exports.create = function (req, res) {
    const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization);

    if (!req.body.enlace | !req.body.descripcion) {
        return res.status(400).json({
            status: 'fail', data: 'I need enlace, descripcion and idUsuario'
        });
    } else {

        Usuario.findById(headerTokenDecoded.data._id, (err, usuario) => {
            if (err) { return res.status(500).json({ status: 'error', data: err }) }

            if (usuario) {
                enlace = new Enlace({
                    _id: new mongoose.Types.ObjectId(),
                    enlace: req.body.enlace,
                    idUsuario: headerTokenDecoded.data._id,
                    descripcion: req.body.descripcion,
                    shortLink: tools.shortLink(new Date().getTime() + req.body.enlace)
                })

                enlace.save((err, enlace) => {
                    if (err) { return res.status(500).json({ status: 'error', data: err }) }

                    return res.status(201).json({ status: 'success', data: enlace })
                })
            } else {
                return res.status(404).json({ status: 'fail', data: 'No user for this token' })
            }
        })
    }

    console.log('\x1b[33m%s\x1b[0m', '[Enlace] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));
}

exports.update = function (req, res) {
    const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization)

    if (!req.body.enlace | !req.body.descripcion) { return res.status(400).json({ reason: 'I need enlace & description' }) }

    Enlace.findById(req.params.id, (err, enlace) => {
        if (err) { return res.status(404).json({ status: 'error', data: err }) }

        if (enlace) {
            if (headerTokenDecoded.data._id == enlace.idUsuario) {
                enlace.enlace = req.body.enlace;
                enlace.descripcion = req.body.descripcion;

                enlace.save((saveErr, updateEnlace) => {
                    if (updateEnlace) {
                        return res.status(200).json({ status: 'success', data: updateEnlace })
                    }
                })
            } else {
                return res.status(401).json({ status: 'fail', data: 'You aren\'t owner of this enlace' })
            }
        } else {
            return res.status(404).json({ status: 'fail', data: 'No enlace for this Id' })
        }
    })

    console.log('\x1b[34m%s\x1b[0m', '[Enlace] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));
}

exports.delete = function (req, res) {
    const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization);

    Enlace.findById(req.params.id, (err, enlace) => {
        if (err) { return res.status(500).json({ status: 'error', data: err }) }

        if (enlace) {
            if (headerTokenDecoded.data._id == enlace.idUsuario) {
                enlace.remove();
                return res.status(200).json({ status: 'success', data: 'enlace has been deleted' })
            } else {
                return res.status(401).json({ status: 'fail', data: 'You aren\'t the owner of this enlace' })
            }
        } else {
            return res.status(404).json({ status: 'fail', data: 'No enlace for that Id' })
        }
    });

    console.log('\x1b[31m%s\x1b[0m', '[Enlace] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));
}

exports.daleteAll = function (req, res) {
    const headerTokenDecoded = tools.jwtDecodeData(req.headers.authorization);
    Enlace.deleteMany({ idUsuario: headerTokenDecoded.data._id }, (err, deleted) => {
        if (err) {
            return res.status(500).json({ status: 'fail', data: err })
        }

        return res.status(204).json({ status: 'success', data: 'All enlaces has been deleted' })
    })

    console.log('\x1b[31m%s\x1b[0m', '[Enlace] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));
}