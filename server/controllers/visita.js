const Visita = require('../models/visita')
const Enlace = require('../models/enlace')
const mongoose = require('mongoose')
const dateFormat = require('dateformat')
const uaParser = require('ua-parser-js'); //First
const useragent = require('express-useragent'); //For Unknow
const tools = require('./tools')
var path = require('path')

exports.get = function(req, res) {
    Visita.findById(req.params.id, function (err, visita) {
        if(err) return res.status(500).json({ status: 'error', data: err })

        if(visita) {
            return res.status(200).json({ status: 'success', data: visita })
        } else {
            return res.status(404).json({ status: 'fail', data: 'There are not visitas for this Id' })
        }
    })

    console.log('\x1b[32m%s\x1b[0m','[Visita] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));
}

// 📌 Deja que deseaar, cambiar cuando se pueda
exports.getAll = function(req, res) {
    console.log('entra')
    Visita.find({}, function(err, visitas) {
        if(err) return res.status(500).json({ status: "error", data: err });  
    })
    if(tools.isAdmin) {
        try {
            Visita.find({}, function(err, visitas){
                if(err) return res.status(500).json({ status:"error", data: err})
                
                console.log('\x1b[32m%s\x1b[0m', 'Type of devices');
                for(visita of visitas) {
                    device = uaParser(visita.user_agent).device.type
                    if(device == undefined) {
                        device = 'Bot: ' + useragent.parse(visita.user_agent).isBot
                    }
                    visita.dispositivo = device;
                    console.log(`Device ${device}, OS: ${useragent.parse(visita.user_agent).os}, platform: ${useragent.parse(visita.user_agent).platform} & model: ${uaParser(visita.user_agent).device.model}`)
                }
                console.log('\x1b[32m%s\x1b[0m', 'Its\'t from');
                for(visita of visitas) {
                    console.log(visita.ip_data.continent_code)
                }

                return res.status(200).json({status: "sucess", data: visitas})
            })   
        } catch (error) {
            return res.status(500).json({ failed: 'internal error'})
        }
    } else {
        try {
            data = tools.jwtDecodeData(req.headers.autorization);
            Visita.find({_id: data._id}, function(err, visitas){
                if(err) return res.status(400).json({failed: err})
    
                return res.status(200).json({sucess: visitas})
            })   
        } catch (error) {
            return res.status(500).json({ failed: 'internal error'})
        }
    }

    console.log('\x1b[32m%s\x1b[0m','[Visita] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));
}


exports.delete = function(req, res) {
    console.log('\x1b[31m%s\x1b[0m','[Visita] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));
    
    Visita.findByIdAndDelete(req.params.id, function (err, visita) {
        if(err) return res.status(400).json({ status: "fail", data: err.message })

        if(visita) { return res.status(204).json({ status: "sucess" }) }
        else { return res.status(204).json({ status: "fail", data: "No enlaces for this Id" }) }
    })
}

exports.daleteAll = function(req, res) {
    console.log('\x1b[31m%s\x1b[0m','[Visita] in ' + dateFormat(new Date(), 'dd-mm-yyyy "at" hh:mm:ss'));

    try {
        visitas.deleteMany({}, function(err, sucess) {
            if(err) return res.status(400).json({ failed: err.message})

            if(sucess) {
                return res.status(200).json({ sucess: 'now visitas is empty'}, sucess)
            } else { return res.status(200).json({ sucess: 'nothing for remove'}) }
        })
    } catch (error) {
        return res.status(500).json({ failed: 'internal server error', err: error})
    }
}

// 📌 Comprobar IP de manera externa y añadir la ip_data
exports.redirect = function(req, res) {
    Enlace.findOne({ shortLink: req.params.uri }, (err, found) => {
        if(err) { return res.status(500).json({ failed: 'Algo falló'}) }
        if(!found) {
            return res.status(404).sendFile(path.join(__dirname + '/../assets/Web/notFound.html'))
        } else {
            res.redirect(301, found.enlace);

            visita = new Visita();
            visita._id = new mongoose.Types.ObjectId()
            //visita.ip = req.ip
            if(tools.apify) { if(res) {visita.ip = res}}
            visita.user_agent = req.header('User-Agent')

            tools.apify().then(function(res) {
                visita.ip = res
                console.log(res)
                tools.ipData(res).then(function(res) {
                    visita.ip_data.continent_code = res.continent_code
                    visita.ip_data.continent_name = res.continent_name
                    visita.ip_data.country_code = res.country_code
                    visita.ip_data.country_name = res.country_name
                    visita.ip_data.region_code = res.region_code
                    visita.ip_data.region_name = res.region_name
                    visita.ip_data.city = res.city
                    visita.ip_data.zip = res.zip
                    visita.ip_data.latitude = res.latitude
                    visita.ip_data.longitude = res.longitude
                    visita.ip_data.location.capital = res.location.capital
                    visita.ip_data.location.languages = res.location.languages
                    visita.ip_data.location.country_flag = res.location.country_flag
                }).then(() => {
                    visita.save((err, saved) => {
                        if(err) { console.log('ERROR' + err) }
                        if(saved) { 
                            found.visitas.push(saved._id)
                            found.save(); console.log('Bitly')
                        }
                    })
                }).catch(() => {
                    console.log('Cannot access to IpStack.com!')
                })
            }).catch(() => {
                console.log('Cannot access to Apify.org!')
            })
        }
    })
}
